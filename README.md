# Cockpit CLI

This is a simple Command Line Interface for the Cockpit protocol.

It can be used to debug Core (via a child process) without writing
JSON in the terminal.

## Setting up the cli

**NB:** _You need Node.JS (version > 18) and npm_

You just need to install the npm packages and you are good to go !

```sh
npm i
```

## Running the CLI

To launch the app, run `npm run test`.

### With a fake Core 

This CLI includes a few lines of code to debug it without the need to 
bring up Core.

This is useful when designing a new pretty print for a specific type of
packet.

### With the `core` executable

To use this CLI with the core executable, set the environment variable
`CORE_BUILD` to the **absolute path** to core's build folder.

First example: on command launch.

```sh
CORE_BUILD=/home/foo/core/build npm run test
```

Second example: exporting the variable.
```sh
export CORE_BUILD=/home/foo/core/build
npm run test
```

**NB:** *The second example has the advantage to not require setting the variable each time you launch the program.*

**YOU NEED TO EXIT THE CLI USING `CTRL+C` TO PROPERLY CLOSE CORE AND DELETE THE SEMAPHORES**

