import { spawn } from "child_process";
import { Packet } from "./Cockpit";
import { CoreHook } from "./dummy";
import readline from "node:readline/promises";

export class CoreProcess implements CoreHook {

    private process;
    private rl;

    callback: (data: Object) => void = () => {};
    

    constructor (readonly cwd: string) {

        this.process = spawn(cwd + "/core", {
            cwd
        });

        this.rl = readline.createInterface(this.process.stdout, this.process.stdin);

        this.rl.on("line", ln => {
            let o = JSON.parse(ln);
            // console.log(o);
            this.callback(o);
        });

    }

    send (data: Packet) {

        this.process.stdin.write(JSON.stringify(data) + "\n");

    }

    stop () {

        this.process.stdin.end();
        this.rl.close();

    }

}