/** A Logo that can be printed to the terminal */
export type Logo = {
    /** The lines that composes the logo */
    logo: string[],
    /** The width of the logo */
    width: number,
    /** The height of the logo */
    height: number
}

/**
 * This logo depicts a rocket lifting-off since the 
 * theme of the 2024 edition of the CFR is "Farm on Mars".
 * 
 * @returns The logo for the 2023-2024 version of Core
 */
export function logo2024 (): Logo {
    let b = "\x1b[1m\x1b[38;5;255m",
        g = "\x1b[1m\x1b[38;5;244m",
        o = "\x1b[1m\x1b[38;5;9m",
        c = "\x1b[0m";

    let logo = [
        `${b}         _         `,
        `${b}        /_\\        `,
        `${b}        | |        `,
        `${b}        |_|        `,
        `${b}       /| |\\       `,
        `${b}      /_| |_\\      `,
        `${b}      | | | |      `,
        `${b}      | | | |      `,
        `${b}      |_|_|_|      `,
        `${g} *    ${b}/8\\ /8\\    ${g}* `,
        `${g}**    ${o}''' '''    ${g}**`,
        `${g}**** * ${o}' ${g}* ${o}' ${g}* ****`,
        `${g} ***** ${o}'${g}***${o}' ${g}***** `,
        `${b}    2023 - 2024    `,
        "                   "
    ].map(e => e + c);
    let width = 19;

    return { logo, width, height: logo.length }
}

/**
 * This logo contains the writing "Core" in ascii-art
 * and the reference to the version number, name.
 * 
 * It also referneces Polybot and the school year.
 * 
 * @param version The version string
 * @param versionName The version name
 * @returns Core's logo
 */
export function asciiLogo (version: string = "0.0.0", versionName: string = "Prototypage"): Logo {
    let txt = 
`   ______                 
  / ____/____   _____ ___ 
 / /    / __ \\ / ___// _ \\
/ /___ / /_/ // /   /  __/
\\____/ \\____//_/    \\___/ 

Version ${version} - ${versionName}
Polybot Grenoble, Année 2023-2024`;

    let logo = txt.split("\n");
    let width = logo.reduce((p, c) => Math.max(p, c.length), 0);
    let height = logo.length;

    logo = logo.map(e => "\x1b[1m" + e + " ".repeat(width - e.length) + "\x1b[0m");

    return { logo, width, height };
}

/**
 * Polytech Grenoble's brown P 
 * 
 * @returns The P form Polytech
 */
export function logoP(): Logo {
    let txt = `  ⣤⣿⣿⣿⣿⣿⣤
⣤⣿⣿⠛   ⠛⣿⣿⣤
⠛⠛       ⣿⣿
⣤⣤      ⣤⣿⣿
⣿⣿⣿⣤⣤⣤⣤⣿⣿⠛
⣿⣿ ⠛⠛⠛⠛⠛
 ⠛`;

    let logo = txt.split("\n");
    let width = logo.reduce((p, c) => Math.max(p, c.length), 0);
    let height = logo.length;

    logo = logo.map(e => "\x1b[1m\x1b[38;5;130m" + e + " ".repeat(width - e.length) + "\x1b[0m");

    return { logo, width, height };

}
 
/**
 * Merges two logos horizontally, and vertically centers them.
 *  
 * @param a The left logo
 * @param b The right logo
 * @param gap The gap to leave between the logos
 * @param pad_x The horizontal padding
 * @param pad_y The vertical padding
 * @param frame Enables the framing around the merged logos
 * @returns The merged logos
 */
export function mergeLogos (a: Logo, b: Logo, gap = 3, pad_x = 2, pad_y = 0, frame = true): Logo {

    let n = Math.max(a.height, b.height);
    let a_start = ~~((n/2) - (a.height / 2));
    let b_start = ~~((n/2) - (b.height / 2));
    let w = a.width + b.width + gap + 2 * pad_x;
    
    let lns = [];
    for (let i = 0; i < n; i++) {
        let ln = `${" ".repeat(pad_x)}${a.logo[i - a_start] || ' '.repeat(a.width)}${" ".repeat(gap)}${b.logo[i - b_start] || " ".repeat(b.width)}${" ".repeat(pad_x)}`;
        lns.push(ln);
    }
    
    for (let i = 0; i < pad_y; i++) {
        lns.unshift(" ".repeat(w));
        lns.push(" ".repeat(w));
    }

    if (frame) {
        lns = lns.map(e => `║${e}║`);
        lns.unshift(`╔${"═".repeat(w)}╗`);
        lns.push(`╚${"═".repeat(w)}╝`);
        w += 2
    }

    return {
        logo: lns,
        width: w,
        height: lns.length
    }

}

/**
 * Removes the ansi escape codes to allow exporting 
 * colored text into a text file.
 * 
 * @param str Text containing escape codes
 * @returns Text without escape codes
 */
export function escapeAnsiCodes (str: string) {
    return str.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, "");
}
