import { CockpitClient, Packet } from "./Cockpit";
import { asciiLogo, logo2024, mergeLogos } from "./core-deco";
import { CoreHook, DummyCore } from "./dummy";
import { Logger } from "./logger";
import { env, exit, stdin, stdout } from "node:process";
import readline from "node:readline/promises";
import { PrettyPrint } from "./prettyprint";
import { CoreProcess } from "./coreProcessTester";

const logger = new Logger("fr", "Europe/Paris", "logs", true);

const banner = mergeLogos(logo2024(), asciiLogo("0.0.1")).logo.join("\n");
logger.log("\n" + banner);

let folder = env["CORE_BUILD"];

const core: CoreHook = folder ? new CoreProcess(folder) : new DummyCore();
const client = new CockpitClient("cli", d => core.send(d), logger);
core.callback = o => client.recieve(o);

const pretty = new PrettyPrint();

const rl = readline.createInterface(stdin, stdout);

const request = (command: string) => new Promise<Packet | undefined>((res, _) => {
    logger.debug(">>", command);

    let resolved = false;

    client.request(command)
        .then(packet => {
            if (resolved) return;
            resolved = true;
            res(packet);
        })
        .catch((reason) => {
            if (resolved) return;
            resolved = true;
            logger.error(`Request "${command}" failed:`, reason);
            res(undefined);
        });
});

const quit = async () => {

    logger.log("Quitting app, o7");
    client.send({ client: "", uid: "", payload: "", type: "stop" })

    core.stop();
    rl.close();
    exit(0);
}

rl.on("SIGINT", quit);

const main = async () => {

    while (true) {

        let line = await rl.question(">> ");
        if (line == "") line = "help";

        let result = await request(line);

        if (result !== undefined) {

            let message = pretty.print(result);
            if (result.type == "error") {
                logger.error(message);
            } else {
                logger.log(message);
            }

        }

    }

}

main();