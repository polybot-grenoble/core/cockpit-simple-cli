import { Packet } from "./Cockpit";

export interface CoreHook {
    callback: (data: Object) => void;
    send: (data: Packet) => void;
    stop: () => void;
}

export class DummyCore implements CoreHook {

    public callback: (data: Object) => void = () => {};

    send (data: Packet) {

        let response: Packet = {
            client: data.client,
            uid: data.uid,
            payload: "",
            type: "",
        };

        switch (data.payload) {

            case "help":
                response.type = "help";
                response.payload = "{\"description\":\"The robot's core program\",\"lines\":[{\"args\":[{\"name\":\"a\",\"required\":true},{\"name\":\"b\",\"required\":false}],\"description\":\"A test command\",\"id\":\"test\",\"type\":\"cmd\"},{\"args\":[],\"description\":\"A nested router\",\"id\":\"subroute\",\"type\":\"route\"}],\"title\":\"Robot\"}";
                this.callback(response);
                break;

            case "cmd":
                response.type = "error";
                response.payload = "{\"code\":103,\"message\":\"Not yet implemented\"}";
                this.callback(response);
                break;
            
            case "to":
                break;

            default:
                response.type = "error";
                response.payload = "{\"code\":100,\"message\":\"Not found\"}";
                this.callback(response);
                break;               

        }

    }

    stop () {
        
    }

}