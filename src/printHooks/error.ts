import { Packet } from "../Cockpit";
import { niceBox } from "./box";

type ErrorMessage = {
    code: number, 
    message: string
}

export function printError (packet: Packet): string {

    let msg = <ErrorMessage>JSON.parse(packet.payload);
    return niceBox(`Error code ${msg.code}`, [[[msg.message]]], "\x1b[37;41m");

}