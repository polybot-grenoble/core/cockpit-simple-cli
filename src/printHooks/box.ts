export function niceBox (rawTitle: string, rawSections: string[][][], style = "") {

    let title = `╡ ${rawTitle} ╞`;
    let minW = title.length + 2;
    
    let sections = rawSections.map(formatSection);
    minW = sections.reduce((w, s) => Math.max(w, s[0].length), minW);
    
    let emptyLine = `\n${style}║ ${" ".repeat(minW)} ║\x1b[0m\n`;
    let sepLine = `${emptyLine}${style}╟${"─".repeat(minW + 2)}╢\x1b[0m${emptyLine}`;

    let body = sections.map(
        s => s.map(
            ln => `${style}║ ${ln.padEnd(minW, " ")} ║\x1b[0m`
        ).join("\n")
    ).join(sepLine);
    
    body = emptyLine + body + emptyLine;

    let halfTitleWidth = ~~((minW + 2 - title.length) / 2);
    let topLine = `\n${style}╔${
        "═".repeat(halfTitleWidth + (minW % 2 - title.length % 2))
        }${title}${
        "═".repeat(halfTitleWidth)
        }╗\x1b[0m`;
    
    let bottomLine = `${style}╚${"═".repeat(minW + 2)}╝\x1b[0m\n`

    return topLine + body + bottomLine;

}

function formatSection (section: string[][]): string[] {

    let colNumber = section[0].length;

    for (let i = 0; i < colNumber; i++) {
        let colWidth = section.reduce((p, s) => Math.max(p, s[i].length), 0);
        section = section.map(ln => {
            ln[i] = ln[i].padEnd(colWidth, " ");
            return ln;
        });
    }

    return section.map(ln => ln.join(" "));

}

