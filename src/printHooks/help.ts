import { Packet } from "../Cockpit";
import { niceBox } from "./box";

type HelpArg = {
    required: boolean,
    name: string
};

type HelpLine = {
    id: string,
    type: 'cmd' | 'route',
    description: string,
    args: HelpArg[]
};

type HelpMessage = {
    title: string,
    description: string,
    lines: HelpLine[]
};

export function printHelp (packet: Packet): string {

    let help = <HelpMessage>JSON.parse(packet.payload);

    let title = `${help.title} - ${help.description}`;

    let routes = help.lines.filter(l => l.type == "route").map(formatLine);
    let cmds = help.lines.filter(l => l.type == "cmd").map(formatLine);

    let msg: string[][][] = [];
    if (routes.length) msg.push(routes);
    if (cmds.length) msg.push(cmds);

    return niceBox(title, msg, "\x1b[37m\x1b[48;5;33m");

}

function formatLine (ln: HelpLine) {
    let args = ln.args.length ? " " + ln.args.map(formatArg).join(" ") : "";
    return [`${ln.id}${args}:`, ln.description];
}

function formatArg (arg: HelpArg) {
    return arg.required ? `<${arg.name}>` : `[${arg.name}]`;
}