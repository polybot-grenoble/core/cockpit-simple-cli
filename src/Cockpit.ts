import { randomUUID } from "node:crypto";
import { Logger } from "./logger";

export type Packet = {
    uid: string,
    client: string,
    type: string,
    payload: string
};

export type CockpitSend = (data: Packet) => void;
export type CockpitRequest = {
    uid: string,
    returns: CockpitSend
}

export class CockpitClient {

    private pending: CockpitRequest[] = [];
    public timeout: number = 2000;

    constructor (
        readonly id: string,
        readonly send: CockpitSend,
        private logger: Logger
    ) {

    }

    recieve (data: Object): void {

        if (!isPacket(data)) {
            // Log error
            this.logger.error("Invalid packet recieved :", data);
            return;
        }

        let p: Packet = <Packet>data;

        if (isLog(p)) {
            // Log
            switch (p.type) {
                case "log_error":
                    this.logger.error(p.payload);
                    break;
                case "log_debug":
                    this.logger.debug(p.payload);
                    break;
                default:
                    this.logger.log(p.payload);
            }
            return;
        }

        let req = this.pending.filter(e => e.uid == p.uid)[0];

        if (!req) {
            // Packet recieved without a request
            this.logger.debug("Recieved message from cockpit :", p);
            return;
        }

        this.pending = this.pending.filter(r => r != req);

        req.returns(p);

    }

    request (command: string): Promise<Packet> {
        
        let uid = randomUUID();
        let packet = this.packet(uid, "request", command);
        
        return new Promise((resolve, reject) => {
            
            let to = setTimeout(() => {
                reject("Timeout");
            }, this.timeout);

            let returns = (data: Packet) => {
                clearTimeout(to);
                resolve(data);
            }
            this.pending.push({ uid, returns });

            this.send(packet);

        });

    }

    packet (uid: string, type: string, payload: string): Packet {
        return {
            uid, type, payload,
            client: this.id
        }
    }

}

export function isPacket (data: Object): boolean {
    return (
           ("uid" in data) && (typeof data["uid"] == "string")
        && ("client" in data) && (typeof data["client"] == "string")
        && ("type" in data) && (typeof data["type"] == "string")
        && ("payload" in data) && (typeof data["payload"] == "string")
    );
}

function isLog (data: Packet): boolean {
    return (
           (data.type == "log")
        || (data.type == "log_error")
        || (data.type == "log_debug")
    );
}